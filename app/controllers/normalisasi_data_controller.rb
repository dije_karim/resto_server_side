class NormalisasiDataController < ApplicationController
  def data_normalize
  	@data = []
  	file = File.new("#{Rails.root}/public/1_normalisasi_data/#{params[:data_txt]}.txt", "r")
    while (line = file.gets)
    	if line != "\r\n"
        if params[:data_txt] == "iris"
  	    	data_txt = line.split(",")[0..3]
        else
          data_txt = line.split("\t")[0..4]
        end
	    	data_txt = data_txt.map { |e| e.to_f }
	    	@data.push(data_txt)
    	end
    end
    file.close

    @min_max_datas = count_min_max(@data, @data.first.length)
    @zscore_datas = count_zscore(@data, @data.first.length)
    @decimal_scaling_datas = count_decimal_scaling(@data, @data.first.length)
    @sigmoidal_datas = count_sigmoidal(@data, @data.first.length)
    @softmax_datas = count_softmax(@data, @data.first.length)
  end

  private
  def re_array(datas, count_column)
    @selected_data = []
    count_column.times do |i|
      data2 = []
      datas.each do |data|
        data2.push(data[i])
      end
      @selected_data.push(data2)
    end

    return @selected_data
  end

  def count_min_max(datas, count_column)
    newmin = 0
    newmax = 1
    @selected_data = re_array(datas, count_column)

    @minmax_value = []
    count_column.times do |idx|
      @minmax_value.push(@selected_data[idx].minmax)
    end

    @min_max_datas = []
    datas.each do |data|
      newdata = []
      data.each_with_index do |data2, jdx|
        newdata.push( ( (data2-@minmax_value[jdx][0]) * (newmax - newmin) ) / ( (@minmax_value[jdx][1] - @minmax_value[jdx][0]) + newmin ) )
      end

      @min_max_datas.push(newdata)
    end

    return @min_max_datas
  end

  def count_zscore(datas, count_column)
    @selected_data = re_array(datas, count_column)

    @avg_datas = []
    @std_deviation = []
    @selected_data.each do |data|
      total = 0
      data.each do |data2|
        total += data2
      end
      avg = total/data.length

      total_kuadrat = 0
      data.each do |data2|
        total_kuadrat += (data2 - avg) ** 2
      end
      variance = total_kuadrat / (data.length - 1)
      std_dev = Math.sqrt(variance)
      @avg_datas.push( avg )
      @std_deviation.push( std_dev )
    end

    @zscore_datas = []
    datas.each do |data|
      newdata = []
      data.each_with_index do |data2, jdx|
        newdata.push( (data2 - @avg_datas[jdx])/@std_deviation[jdx] )
      end
      @zscore_datas.push( newdata )
    end

    return @zscore_datas
  end

  def count_decimal_scaling(datas, count_column)
    if params[:data_txt] == "iris"
      pengali = [-1, -1, -1, -1]
    else
      pengali = [2, 1, 0, 1, 1]
    end

    decimal_scaling_datas = [pengali]

    datas.each do |data|
      decimal_scaling = []
      data.each_with_index do |data2, jdx|
        decimal_scaling.push( data2 / ( 10**(pengali[jdx]) ) )
      end

      decimal_scaling_datas.push( decimal_scaling )
    end

    return decimal_scaling_datas
  end

  def count_sigmoidal(datas, count_column)
    data_zscore = count_zscore(datas, count_column)

    sigmoidal_datas = []
    data_zscore.each do |zscores|
      sigmoidal = []
      zscores.each do |zscore|
        sigmoidal.push( (1 - (Math.exp(-zscore)))/ (1 + (Math.exp(-zscore))) )
      end
      sigmoidal_datas.push(sigmoidal)
    end

    return sigmoidal_datas
  end

  def count_softmax(datas, count_column)
    dev_linier = 1

    @selected_data = re_array(datas, count_column)

    @avg_datas = []
    @std_deviation = []
    @selected_data.each do |data|
      total = 0
      data.each do |data2|
        total += data2
      end
      avg = total/data.length

      total_kuadrat = 0
      data.each do |data2|
        total_kuadrat += (data2 - avg) ** 2
      end
      variance = total_kuadrat / (data.length - 1)
      std_dev = Math.sqrt(variance)
      @avg_datas.push( avg )
      @std_deviation.push( std_dev )
    end

    softmax_datas = []
    datas.each do |data|
      softmax = []
      data.each_with_index do |data2, jdx|
        transfdata = (data2 - @avg_datas[jdx]) / (dev_linier * (@std_deviation[jdx] / (2 * 3.14)) )
        softmax.push( 1/(1 + Math.exp(-transfdata)) )
      end

      softmax_datas.push( softmax )
    end

    return softmax_datas
  end
end
