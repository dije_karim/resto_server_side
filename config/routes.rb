Rails.application.routes.draw do
  get 'normalisasi_data/data_normalize'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'welcome/index'
  root 'welcome#index'
end
